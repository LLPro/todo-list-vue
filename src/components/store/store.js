import Vue from 'vue'
import Vuex from "vuex"
import axios from 'axios'

Vue.use(Vuex,axios);

export default new Vuex.Store({
    state:{
        todoList:[],
        ls:[]
    },
    getters: {
        done(state){
            return state.ls;
        }
    },
    mutations: {
        addToStore(state,payload) {
            state.todoList.push(payload);
        },
        createBaseMigration(state,payload){
            state.ls = payload;
        },
    },
    actions: {
        createBaseAction({commit}){
            let loc = null;
            axios.get("https://jsonplaceholder.typicode.com/todos")
                .then(response => {
                        commit('createBaseMigration', response.data);
                        loc = response.data
                    }
                )
                .then((response) =>
                    axios.post("http://localhost:3000/ls",loc)
                        .then(res => res.data)
                        .catch(err=> console.log(err))
                )
                .catch(err=>console.log(err));
        },
        saveTodo({commit},payload){
            axios.post("http://localhost:3000/ls",payload)
                .then(response=>response.data)
                .then(()=>commit('addToStore',payload))
        }
    },
});