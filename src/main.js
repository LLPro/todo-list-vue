import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from "./components/route/router"
import VueResource from 'vue-resource'
import Vuex from 'vuex'
import store from './components/store/store'

Vue.use(VueRouter,VueResource,Vuex);

new Vue({
  el:'#app',
  router,
  store,
  render: h => h(App),
});